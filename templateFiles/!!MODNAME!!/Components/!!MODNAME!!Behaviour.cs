using UnityEngine;

namespace !!MODNAME!!.Components;

//This class left intentionally empty
//All behaviours in the mod inherit from this class, can be used to easily (and dirtily) clean up when the mod is disabled
public class !!MODNAME!!Behaviour : MonoBehaviour
{
}