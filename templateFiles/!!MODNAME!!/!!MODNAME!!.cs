﻿using System;
using System.Security.Permissions;
using BepInEx;
using BepInEx.Logging;
using !!MODNAME!!.Components;
using !!MODNAME!!.Configuration;
using !!MODNAME!!.Hooks;
using MonoMod.RuntimeDetour.HookGen;
using R2API.Networking;
using R2API.Utils;

#pragma warning disable CS0618 // Type or member is obsolete
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
#pragma warning restore CS0618 // Type or member is obsolete

namespace !!MODNAME!!;

[BepInPlugin("com.kuberoot.!!MODNAMELOWER!!", "!!MODNAME!!", "1.0.0")]
[NetworkCompatibility]
[BepInDependency(NetworkingAPI.PluginGUID)]
public class !!MODNAME!! : BaseUnityPlugin
{
    public HookManager HookManager;

    internal ManualLogSource _logger => Logger;
    public static !!MODNAME!! Instance { get; private set; }
    public Config ModConfig { get; private set; }

    public void Awake()
    {
        ModConfig = new Config(this, Logger);
        HookManager = new HookManager(this);
    }

    public void Start()
    {
        ModConfig.Init();
    }

    public void OnEnable()
    {
        Instance = this;

        HookManager.RegisterHooks();
    }

    public void OnDisable()
    {
        if (Instance == this) Instance = null;

        HookManager.UnregisterHooks();

        // Cleanup any leftover hooks
        HookEndpointManager.RemoveAllOwnedBy(HookEndpointManager.GetOwner((Action)OnDisable));

        foreach (var component in FindObjectsOfType<!!MODNAME!!Behaviour>())
            if (component != null)
                Destroy(component);
    }
}