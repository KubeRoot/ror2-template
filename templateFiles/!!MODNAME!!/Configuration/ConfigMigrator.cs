using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BepInEx.Configuration;

namespace !!MODNAME!!.Configuration;

public class ConfigMigrator
{
    private static readonly PropertyInfo Property_ConfigFiles_OrphanedEntries =
        typeof(ConfigFile).GetProperty("OrphanedEntries",
            BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

    private static readonly Dictionary<ConfigDefinition, Action<Config, string>> migrations = new()
    {
        // {new("General", "ExampleOldEntry"), (config, value) => config.exampleNewEntry.Value = value == "true" },
    };

    private readonly ConfigFile config;
    private readonly Config ModConfig;
    private readonly Dictionary<ConfigDefinition, string> OrphanedEntries;

    public ConfigMigrator(ConfigFile config, Config modConfig)
    {
        this.config = config;
        ModConfig = modConfig;
        OrphanedEntries = (Dictionary<ConfigDefinition, string>)Property_ConfigFiles_OrphanedEntries.GetValue(config);
    }

    public bool NeedsMigration => migrations.Keys.Any(def => OrphanedEntries.ContainsKey(def));

    public void DoMigration()
    {
        List<ConfigDefinition> migratedKeys = new();

        foreach (var entry in migrations)
        {
            var def = entry.Key;
            var migration = entry.Value;

            if (OrphanedEntries.TryGetValue(def, out var orphanedEntry))
            {
                migration(ModConfig, orphanedEntry);
                migratedKeys.Add(def); // Don't mutate dictionary while iterating
            }
        }

        foreach (var def in migratedKeys) OrphanedEntries.Remove(def);

        config.Save();
    }
}