using BepInEx.Configuration;
using BepInEx.Logging;

namespace !!MODNAME!!.Configuration;

public class Config
{
    private readonly ManualLogSource logger;
    public readonly !!MODNAME!! Plugin;

    public ConfigMigrator Migrator;

    public ConfigEntry<bool> SampleConfigEntry;

    public Config(!!MODNAME!! plugin, ManualLogSource _logger)
    {
        Plugin = plugin;
        logger = _logger;

        Migrator = new ConfigMigrator(config, this);

        config.SettingChanged += ConfigOnSettingChanged;

        Migrator.DoMigration();
    }

    private ConfigFile config => Plugin.Config;

    public void Init()
    {
        SampleConfigEntry = config.Bind("General", "SampleConfigEntry", false,
            "Should pickup pickers be shared?\nIf true, pickup pickers (such as void orbs and command essences) will be shared among the players they are instanced for.\nA shared pickup picker can only be opened by one player, and will then drop an item that can be picked up separately.\nIf a pickup picker is not shared, then the item can be selected separately by each player.");
    }

    private void ConfigOnSettingChanged(object sender, SettingChangedEventArgs e)
    {
        // Clear any cached data
    }
}